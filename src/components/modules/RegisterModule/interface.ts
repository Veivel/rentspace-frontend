export interface IRegisterData {
  username: string
  password: string
  repeatedPassword: string
  role: 'ADMIN' | 'CUSTOMER' | 'PROVIDER'
}
