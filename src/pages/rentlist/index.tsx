import { RentlistModule } from '@modules'
import type { NextPage } from 'next'

const Rentlist: NextPage<any> = () => {
  return (
    <>
      <RentlistModule />
    </>
  )
}
export default Rentlist
