import React from 'react'
import { SpacesModule } from '@modules/SpacesModule/Catalogue'
import type { NextPage } from 'next'

const Spaces: NextPage = () => <SpacesModule />

export default Spaces
