This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## Getting Started

1. Make sure you have NPM installed.

2. Install all dependencies:

   ```bash
   npm install
   ```

3. Don't forget to populate your `.env` file. Do NOT push the `.env` file. Use `.env.example` for reference.

4. Run the development server locally:

   ```bash
   npm run dev
   ```

5. Don't forget to run `npm run test-all` before committing.

## Learn More

To learn more about Next.js, take a look at the following resources:

- [Next.js Documentation](https://nextjs.org/docs) - learn about Next.js features and API.
- [Learn Next.js](https://nextjs.org/learn) - an interactive Next.js tutorial.
- [React DaisyUI](https://react.daisyui.com/?path=/story/welcome--page)
