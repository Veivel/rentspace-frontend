import React, { useState, useEffect } from 'react'
import axios from 'axios'
import { useAuthContext } from '@contexts'
import { Rent } from '@modules/DashboardModule/Provider/sections/reservasi/interface'

export const HistoryReservasi: React.FC = () => {
  const { user, isAuthReady } = useAuthContext()
  const [data, setData] = useState<Rent[]>([])
  useEffect(() => {
    if (!isAuthReady) return
    axios
      .get(
        `${process.env.NEXT_PUBLIC_SERVICE_MAIN2_URL}/api/v1/rent/reservation/customer/${user?.id}`
      )
      .then((res) => {
        setData(res.data)
        console.log(res.data)
      })
      .catch((err) => {
        console.log(err)
      })
  }, [isAuthReady])

  return (
    <>
      <div className="w-[30vw] overflow-hidden rounded-lg shadow-xs">
        <div className="w-full overflow-x-auto">
          <table className="w-full text-center whitespace-no-wrap">
            <thead>
              <tr className="text-xs font-medium tracking-wider text-white uppercase bg-slate-500 ">
                <th className="px-6 py-4">Space ID</th>
                <th className="px-6 py-4">Date Start</th>
                <th className="px-6 py-4">Date End</th>
              </tr>
            </thead>
            <tbody className="bg-white divide-y divide-gray-200">
              {data.map((item, index) => (
                <tr key={index}>
                  <td className="px-6 py-4 whitespace-no-wrap">
                    {item.spaceID}
                  </td>
                  <td className="px-6 py-4 whitespace-no-wrap">
                    {item.dateStart}
                  </td>
                  <td className="px-6 py-4 whitespace-no-wrap">
                    {item.dateEnd}
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
    </>
  )
}
