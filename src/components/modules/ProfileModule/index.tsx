import React from 'react'
import { Modals, HistoryTopup } from './module-elements'
import { HistoryReservasi } from './sections/HistoryReservasi'
import { useAuthContext } from '@contexts'

export const ProfileModule: React.FC = () => {
  const { user } = useAuthContext()

  return (
    <>
      <div className="flex flex-col items-center justify-center p-5">
        <div className="text-black w-[45vw] h-full bg-slate-300 rounded-md p-10">
          <div className="flex flex-row items-center justify-between">
            <div className="flex flex-col">
              <h1 className="text-xl font-bold">{user?.username}</h1>
              <h2 className="text-lg font-semibold">{user?.role}</h2>
              <p>Saldo: {}</p>
            </div>
            <Modals />
          </div>

          <div className="divider"></div>

          <div className="flex flex-col justify-between items-center">
            <h2 className="text-lg font-semibold m-1">History Topup</h2>
            <HistoryTopup />
          </div>
        </div>
      </div>
    </>
  )
}
