export interface Props {}

export interface Space {
  id: number
  name: string
  description: string
  image: string
  providerId: number
  type: string
  totalSlots: number
  rentCost: number
  approved: boolean
  openArea: boolean
}

export interface Topup {
  topUpSaldoId: number
  amount: number
}
