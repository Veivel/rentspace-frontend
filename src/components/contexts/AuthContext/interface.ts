export interface User {
  id: number
  username: string
  role: string
  // TODO
}

export interface Jwt {
  refreshToken: string
  accessToken: string
}

export interface IAuthContextProps {
  children: React.ReactNode
}

export interface IAuthContext {
  jwt: Jwt | null
  setJwt: React.Dispatch<any>
  user: User | null
  setUser: React.Dispatch<any>
  isAuthReady: boolean
  setIsAuthReady: React.Dispatch<any>
}
