import React, { useEffect, useState } from 'react'
import { CardSpaces } from './module-elements'
import { Space } from '../interface'
import axios from 'axios'
import { toast } from 'react-hot-toast'
import { Button, Pagination } from 'react-daisyui'
import { useAuthContext } from '@contexts'
import { ToastContainer } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'

export const SpacesModule: React.FC = () => {
  const { user, jwt, isAuthReady } = useAuthContext()
  const [data, setData] = useState<Space[]>([])
  const [pageNum, setPageNum] = useState<number>(0)
  useEffect(() => {
    console.log(user)
    axios
      .get(
        `${process.env.NEXT_PUBLIC_SERVICE_MAIN_URL}/api/v1/spaces?page=${pageNum}`,
        {
          headers: {
            Authorization: jwt?.accessToken,
          },
        }
      )
      .then((res) => {
        console.log(res)
        setData(res.data as Space[])
      })
      .catch((err) => {
        toast.error('Could not fetch data.')
        console.log(err)
      })
  }, [pageNum])

  return (
    <>
      <div className="flex flex-col items-center py-10 min-h-screen">
        <h1 className="text-4xl font-extrabold mt-3 mb-6">SPACES CATALOGUE</h1>
        {user?.role === 'PROVIDER' ? (
          <Button className="mb-7" href="/spaces/create">
            + Create New Space
          </Button>
        ) : (
          <></>
        )}
        <div className="grid grid-cols-3 gap-4">
          {data?.map((space, index) => {
            return <CardSpaces key={index} {...space} />
          })}
        </div>
        <Pagination>
          <Button
            disabled={pageNum === 0}
            onClick={(e) => setPageNum((prev) => prev - 1)}
          >
            Prev
          </Button>
          <Button>{pageNum.toString()}</Button>
          <Button
            disabled={data.length === 0}
            onClick={(e) => setPageNum((prev) => prev + 1)}
          >
            Next
          </Button>
        </Pagination>
      </div>
      <ToastContainer />
    </>
  )
}
