import React from 'react'
import { LogoutModule } from '@modules'
import type { NextPage } from 'next'

const Logout: NextPage = () => <LogoutModule />

export default Logout
