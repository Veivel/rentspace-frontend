import React from 'react'
import type { NextPage } from 'next'
import { AdminDashboardModule } from '@modules/DashboardModule/Admin'
import { ProviderDashboardModule } from '@modules/DashboardModule/Provider'
import { useAuthContext } from '@contexts'

const Dashboard: NextPage = () => {
  const { user } = useAuthContext()
  return user?.role === 'ADMIN' ? (
    <AdminDashboardModule />
  ) : (
    <ProviderDashboardModule />
  )
}

export default Dashboard
