import React, { useState, useEffect, use } from 'react'
// import {HeroSection, FAQSection} from './sections
// import {} from './module-elements'
import { SPACES_API } from './constant'
import axios from 'axios'
import { Space, Topup } from './interface'
import { useAuthContext } from '@contexts'
import secureLocalStorage from 'react-secure-storage'

export const AdminDashboardModule: React.FC = () => {
  // kalo isAuthReady true, berarti jwt udah ada / selesai load
  const user = JSON.parse(secureLocalStorage.getItem('user') as string)
  const jwt = JSON.parse(secureLocalStorage.getItem('jwt') as string)
  const { isAuthReady } = useAuthContext()

  const dummyTopup = [
    {
      nama: 'Andy',
      amount: 10000,
    },
    {
      nama: 'Rey',
      amount: 15000,
    },
    {
      nama: 'Richard',
      amount: 12000,
    },
  ]

  // data topup yang akan di-accept/reject oleh admin
  const [dataTopup, setDataTopup] = useState<Topup[]>([])
  useEffect(() => {
    console.log(jwt)
    axios
      .get(
        `${process.env.NEXT_PUBLIC_SERVICE_MAIN_URL}/api/v1/saldo/topup/history`,
        {
          headers: {
            Authorization: jwt?.accessToken,
          },
        }
      )
      .then((res) => {
        setDataTopup(res.data)
      })
      .catch((err) => {
        console.log(err)
      })
  }, [isAuthReady])

  // data space yang akan di-accept/reject oleh admin
  const [dataSpace, setDataSpace] = useState<Space[]>([])
  useEffect(() => {
    axios
      .get(`${process.env.NEXT_PUBLIC_SERVICE_MAIN_URL}/api/v1/spaces?page=0`)
      .then((res) => {
        setDataSpace(res.data)
      })
      .catch((err) => {
        console.log(err)
      })
  }, [])

  const handleAcceptTopup = () => {
    alert('Topup Accepted')
    axios
      .get(
        `${process.env.NEXT_PUBLIC_SERVICE_MAIN_URL}/api/v1/saldo/verify_topup/1`
      )
      .then((res) => {
        console.log(res)
      })
      .catch((err) => {
        console.log(err)
      })
  }
  const handleRejectTopup = () => {
    alert('Topup Rejected')
  }
  const handleAcceptSpace = () => {
    alert('Space Accepted')
  }
  const handleRejectSpace = () => {
    alert('Space Rejected')
  }

  return (
    <>
      <h1 className="text-center text-4xl font-extrabold m-5">
        DASHBOARD ADMIN
      </h1>
      <div className="w-screen overflow-hidden rounded-lg shadow-xs grid grid-cols-2 px-10">
        {/* TOP UP */}
        <div className="overflow-x-auto m-5">
          <h1 className="text-3xl font-bold text-center mb-2">Top Up</h1>
          <table className="w-full text-center whitespace-no-wrap">
            <thead>
              <tr className="text-xs font-medium tracking-wider text-white uppercase bg-slate-500 ">
                <th className="px-6 py-4">Top Up ID</th>
                <th className="px-6 py-4">Amount</th>
                <th className="px-6 py-4">Action</th>
              </tr>
            </thead>
            <tbody className="bg-white divide-y divide-gray-200">
              {dataTopup.map((item, index) => (
                <tr key={index} className="text-sm text-gray-500">
                  <td className="px-6 py-4">{item.topUpSaldoId}</td>
                  <td className="px-6 py-4">{item.amount}</td>
                  <td className="px-6 py-4">
                    <button
                      onClick={handleAcceptTopup}
                      className="px-4 py-2 text-white bg-violet-500 duration-75 hover:bg-violet-700 hover:scale-105 rounded-md mr-1"
                    >
                      Accept
                    </button>
                    <button
                      onClick={handleRejectTopup}
                      className="px-4 py-2 text-white bg-rose-700 duration-75 hover:bg-rose-700 hover:scale-105 rounded-md ml-1"
                    >
                      Reject
                    </button>
                  </td>
                </tr>
              ))}
              {dataTopup.map((item, index) => (
                <tr key={index} className="text-sm text-gray-500">
                  <td className="px-6 py-4">{item.topUpSaldoId}</td>
                  <td className="px-6 py-4">{item.amount}</td>
                  <td className="px-6 py-4">
                    <button
                      onClick={handleAcceptTopup}
                      className="px-4 py-2 text-white bg-violet-500 duration-75 hover:bg-violet-700 hover:scale-105 rounded-md mr-1"
                    >
                      Accept
                    </button>
                    <button
                      onClick={handleRejectTopup}
                      className="px-4 py-2 text-white bg-rose-700 duration-75 hover:bg-rose-700 hover:scale-105 rounded-md ml-1"
                    >
                      Reject
                    </button>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>

        {/* SPACES */}
        <div className="overflow-x-auto m-5">
          <h1 className="text-3xl font-bold text-center mb-2">Spaces</h1>
          <table className="w-full text-center whitespace-no-wrap">
            <thead>
              <tr className="text-xs font-medium tracking-wider text-white uppercase bg-slate-500 ">
                <th className="px-6 py-4">Nama</th>
                <th className="px-6 py-4">Nama Provider</th>
                <th className="px-6 py-4">Action</th>
              </tr>
            </thead>
            <tbody className="bg-white divide-y divide-gray-200">
              {/* data from api */}
              {dataSpace.map((item: Space, index: number) => (
                <tr key={index} className="text-sm text-gray-500">
                  <td className="px-6 py-4">{item.name}</td>
                  <td className="px-6 py-4">{item.providerId}</td>
                  <td className="px-6 py-4">
                    <button
                      onClick={handleAcceptSpace}
                      className="px-4 py-2 text-white bg-violet-500 duration-75 hover:bg-violet-700 hover:scale-105 rounded-md mr-1"
                    >
                      Accept
                    </button>
                    <button
                      onClick={handleRejectSpace}
                      className="px-4 py-2 text-white bg-rose-700 duration-75 hover:bg-rose-700 hover:scale-105 rounded-md ml-1"
                    >
                      Reject
                    </button>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
    </>
  )
}
