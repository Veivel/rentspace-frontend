import React, { ChangeEvent, useEffect, useState } from 'react'
import { Button, Input, Link } from 'react-daisyui'
import { ILoginData } from './interface'
import { DEFAULT_LOGIN_DATA } from './constant'
import axios from 'axios'
import { useAuthContext } from '@contexts'
import { useRouter } from 'next/router'
import secureLocalStorage from 'react-secure-storage'
import { ToastContainer, toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'

export const LoginModule: React.FC = () => {
  const [form, setForm] = useState<ILoginData>(DEFAULT_LOGIN_DATA)
  const { setUser, setJwt, setIsAuthReady } = useAuthContext()
  const router = useRouter()

  function handleFormChange(
    e: ChangeEvent<HTMLInputElement | HTMLSelectElement>
  ) {
    e.preventDefault()
    setForm((prev) => ({
      ...prev,
      [e.target.name]: e.target.value,
    }))
  }

  function handleFormSubmit(e: any) {
    e.preventDefault()

    const body = {
      username: form.username,
      password: form.password,
    }

    axios
      .post(
        `${process.env.NEXT_PUBLIC_SERVICE_MAIN_URL}/api/v1/auth/login`,
        body
      )
      .then((res) => {
        const { accessToken } = res.data
        setJwt(res)
        secureLocalStorage.setItem('jwt', JSON.stringify(res.data))

        axios
          .get(`${process.env.NEXT_PUBLIC_SERVICE_MAIN_URL}/api/v1/account`, {
            headers: {
              Authorization: accessToken,
            },
          })
          .then((res2) => {
            setUser(res2.data)
            secureLocalStorage.setItem('user', JSON.stringify(res2.data))
          })
          .then((res) => {
            toast.success('Successfully logged in.')
          })
          .then((res) => {
            setTimeout(() => {
              router.push('/')
            }, 2000)
          })
          .catch((err) => {
            toast.error('Could not login. [2]')
          })
      })
      .catch((err) => {
        toast.error('Could not login.')
      })
  }

  return (
    <>
      <main className="min-h-screen">
        <div className="py-16 xl:px-64 lg:px-20 md:px-16 px-6">
          <div className="shadow-lg flex flex-col gap-y-2 py-12 px-24 bg-white rounded-3xl">
            <div></div>
            <h1 className="text-4xl font-extrabold mt-3 mb-7 mx-auto">Login</h1>
            <label>Username</label>
            <Input
              name="username"
              value={form?.username}
              onChange={handleFormChange}
              placeholder="Your username"
            />
            <label>Password</label>
            <Input
              name="password"
              type="password"
              value={form?.password}
              onChange={handleFormChange}
              placeholder="Your password"
            />
            <br />
            <Button onClick={handleFormSubmit}>Submit</Button>
            <br />
            <Link href="/auth/register" className="mx-auto">
              Don{'`'}t have an acccount? Register
            </Link>
          </div>
        </div>
        <ToastContainer />
      </main>
    </>
  )
}
