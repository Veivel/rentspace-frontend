/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ['./src/**/*.{js,ts,jsx,tsx}', './components/**/*.{js,ts,jsx,tsx}'],
  theme: {
    extend: {
      fontFamily: {
        productSans: 'ProductSans',
        productSansBold: 'ProductSansBold',
        productSansBoldItalic: 'ProductSansBoldItalic',
        productSansItalic: 'ProductSansItalic',
      },
    },
  },
  plugins: [require('daisyui')],
  daisyui: {
    themes: ['light'],
  },
}
