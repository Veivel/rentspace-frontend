import React, { useEffect } from 'react'
import { useAuthContext } from '@contexts'
import { ProviderDashboardSpacesModule } from './sections/spaces'
import { ProviderDashboardReservasiModule } from './sections/reservasi'
import { ProviderDashboardHistoryModule } from './sections/history'

export const ProviderDashboardModule: React.FC = () => {
  const { user, jwt, isAuthReady } = useAuthContext()

  return (
    <>
      <div className="w-full flex flex-col justify-center items-center">
        <h1 className="text-4xl text-bold m-5">Provider Dashboard</h1>
        <div className="grid grid-cols-3 gap-5">
          <ProviderDashboardSpacesModule />
          <ProviderDashboardReservasiModule />
          <ProviderDashboardHistoryModule />
        </div>
      </div>
    </>
  )
}
