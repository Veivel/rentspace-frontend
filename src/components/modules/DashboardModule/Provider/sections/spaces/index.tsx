import React, { useState, useEffect } from 'react'
import { useAuthContext } from '@contexts'
import { Space, Rent } from './interface'
import axios from 'axios'
import { Button } from 'react-daisyui'

export const ProviderDashboardSpacesModule: React.FC = () => {
  const { user, jwt, isAuthReady } = useAuthContext()
  const providerId = user?.id
  const [data, setData] = useState<Space[]>([])

  useEffect(() => {
    axios
      .get(`${process.env.NEXT_PUBLIC_SERVICE_MAIN_URL}/api/v1/spaces?page=0`)
      .then((res) => {
        console.log(res)
        setData(res.data as Space[])
      })
      .catch((err) => {
        alert('Could not fetch data.')
        // console.log(err)
      })
  }, [])

  const dummySpaces: Space[] = [
    { id: 60, name: 'Space 1', providerId: 1 },
    { id: 50, name: 'Space 2', providerId: 1 },
    { id: 20, name: 'Space 3', providerId: 1 },
  ]

  return (
    <>
      <div className="flex flex-col justify-center items-center">
        <h2 className="text-2xl font-bold">Daftar Spaces</h2>
        <table className="w-full text-center whitespace-no-wrap">
          <thead>
            <tr className="text-xs font-medium tracking-wider text-white uppercase bg-slate-500 ">
              <th className="px-6 py-4">Space ID</th>
              <th className="px-6 py-4">Action</th>
            </tr>
          </thead>
          <tbody className="bg-white divide-y divide-gray-200">
            {data &&
              data.map((space) => {
                if (providerId === space.providerId) {
                  const path = `/spaces/${space.id}/update`
                  return (
                    <tr key={space.id}>
                      <td>{space.id}</td>
                      <td>
                        <Button href={path}>Edit</Button>
                      </td>
                    </tr>
                  )
                }
              })}
            {dummySpaces.map((item, index) => {
              const path = `/spaces/${item.id}/update`
              return (
                <tr key={index} className="text-sm text-gray-500">
                  <td className="px-6 py-4">dummy {item.id}</td>
                  <td className="px-6 py-4">
                    <Button href={path}>Edit</Button>
                  </td>
                </tr>
              )
            })}
          </tbody>
        </table>
      </div>
    </>
  )
}
