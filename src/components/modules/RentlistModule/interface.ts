export interface RentData {
  id: number
  customerID: number
  spaceID: number
  dateStart: string
  dateEnd: string
  isValid: boolean
}

export interface SpacesData {
  id: number
  name: string
  description: string
  image: string
  providerId: number
  type: string
  totalSlots: number
  rentCost: number
  approved: boolean
  openArea: boolean
  dateStart: string
  dateEnd: string
}
