import React, { useState, useEffect } from 'react'
import { useRouter } from 'next/router'
import { Extra, Space } from '../interface'
import axios from 'axios'
import { useForm, SubmitHandler } from 'react-hook-form'
import { useAuthContext } from '@contexts'
import { Button, Input, Select } from 'react-daisyui'
import { ToastContainer, toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'

type Inputs = {
  id?: number
  providerId?: number
  name?: string
  description?: string
  image?: string
  type?: string
  totalSlots?: number
  rentCost?: number
  openArea?: boolean
}

export const UpdateSpace: React.FC = () => {
  const router = useRouter()
  const { id } = router.query
  const { user, jwt, isAuthReady } = useAuthContext()

  const [data, setData] = useState<Space>()
  const [extras, setExtras] = useState<Extra[]>([])
  const [newName, setNewName] = useState<string>('')

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<Inputs>()

  const handleBack = () => {
    router.back()
  }

  const fetchData = async () => {
    try {
      if (router.isReady) {
        const headers = {
          Authorization: jwt?.accessToken,
        }

        const res = await axios.get(
          `${process.env.NEXT_PUBLIC_SERVICE_MAIN_URL}/api/v1/spaces/${id}`,
          { headers: headers }
        )
        setData(res.data)
        // if (user?.id !== res.data.providerId) {
        //   toast.error('You cannot edit this Space.')
        //   router.push('/spaces')
        // }

        const resExtras = await axios.get(`${process.env.NEXT_PUBLIC_SERVICE_MAIN_URL}/api/v1/spaces/${id}/extras`, {
          headers: headers,
        })
        setExtras(resExtras.data)
      }
    } catch (error) {
      console.log(error)
    }
  }

  useEffect(() => {
    fetchData()
  }, [id])

  async function handleNewService(e: any) {
    e.preventDefault()

    const body = { name: newName }
    if (jwt) {
      axios
        .post(`${process.env.NEXT_PUBLIC_SERVICE_MAIN2_URL}/api/v1/spaces/${id}/extras`, body, {
          headers: {
            Authorization: `${jwt.accessToken}`,
          },
        })
        .then((res) => {
          fetchData()
        })
    } else {
      toast.error(`Could not save new Service.`)
    }
  }

  const onSubmit: SubmitHandler<Inputs> = async (data) => {
    if (!data.description) {
      delete data.description
    }
    if (!data.image) {
      delete data.image
    }
    if (!data.name) {
      delete data.name
    }
    if (!data.openArea) {
      delete data.openArea
    }
    if (!data.rentCost) {
      delete data.rentCost
    }
    if (!data.totalSlots) {
      delete data.totalSlots
    }
    if (!data.type) {
      delete data.type
    }
    delete data.providerId
    delete data.id

    axios
      .patch(`${process.env.NEXT_PUBLIC_SERVICE_MAIN_URL}/api/v1/spaces/${id}`, data, {
        headers: {
          Authorization: `${jwt?.accessToken}`,
        },
      })
      .then((res) => {
        toast.success('Space successfully updated.')
      })
      .catch((err) => {
        toast.error(err.message)
      })
  }

  return (
    <>
      <div className="bg-slate-300 flex flex-row gap-x-4 justify-center items-center h-full p-10">
        <div>
          <button
            onClick={handleBack}
            className="absolute top-20 left-10 bg-slate-500 hover:bg-slate-700 hover:scale-105 duration-100 focus:scale-95 focus:bg-slate-300 px-4 py-2 rounded-xl text-white"
          >
            back
          </button>
          <h1 className="text-3xl font-bold">Update Space</h1>
          <form
            className="w-[30vw] flex flex-col"
            onSubmit={handleSubmit(onSubmit)}
          >
            <input
              type="hidden"
              value={data?.id}
              className="mb-1 rounded-md px-3 py-2"
              {...register('id')}
            />

            <input
              type="hidden"
              value={data?.providerId}
              className="mb-1 rounded-md px-3 py-2"
              {...register('providerId')}
            />

            <label>Name</label>
            <input
              className="mb-1 rounded-md px-3 py-2"
              defaultValue={data?.name}
              {...register('name')}
            />
            {errors.name && <span>This field is required</span>}

            <label>Description</label>
            <input
              className="mb-1 rounded-md px-3 py-2"
              defaultValue={data?.description}
              {...register('description')}
            />
            {errors.description && <span>This field is required</span>}

            <label>Image Link</label>
            <input
              className="mb-1 rounded-md px-3 py-2"
              defaultValue={data?.image}
              {...register('image')}
            />
            {errors.image && <span>This field is required</span>}

            <label>Type</label>
            <Select {...register('type')} className="border border-neutral-200">
              <option value={'BALAI'}>Balai </option>
              <option value={'RUANG_RAPAT'}>Ruang Rapat </option>
              <option value={'RUANG_SIDANG'}>Ruang Sidang</option>
              <option value={'LAPANGAN'}>Lapangan </option>
              <option value={'JALAN_RAYA'}>Jalan Raya </option>
              <option value={'FASILITAS_LAIN'}>Etc.</option>
            </Select>
            {errors.type && <span>This field is required</span>}

            <label>Total Slots</label>
            <input
              type="number"
              className="mb-1 rounded-md px-3 py-2"
              defaultValue={data?.totalSlots}
              {...register('totalSlots')}
            />
            {errors.totalSlots && <span>This field is required</span>}

            <label>Rent Cost</label>
            <input
              type="number"
              className="mb-1 rounded-md px-3 py-2"
              defaultValue={data?.rentCost}
              {...register('rentCost')}
            />
            {errors.rentCost && <span>This field is required</span>}

            <label>Open Area</label>
            <select
              className="mb-1 rounded-md px-3 py-2"
              {...register('openArea')}
            >
              <option value="true">Yes</option>
              <option value="false">No</option>
            </select>

            <Button type="submit">SAVE Space data</Button>
          </form>
        </div>
        <form className="mb-auto pt-8">
          <h1>Create Extra Service:</h1>
          <div className="flex gap-x-1">
            <Input
              value={newName}
              onChange={(e) => setNewName(e.target.value)}
            />
            <Button onClick={handleNewService}>Create Service</Button>
          </div>
          <h1>
            <b>Extra services provided:</b>
          </h1>
          <div className="grid grid-cols-3 gap-1">
            {extras?.map((extra, idx) => (
              <div
                key={idx}
                className="p-1 bg-blue-900 text-white text-sm font-light text-center rounded-2xl w-[150px]"
              >
                <p>{extra.name}</p>
              </div>
            ))}
          </div>
        </form>
        <ToastContainer />
      </div>
    </>
  )
}
