import React from 'react'
import { ISpaceCardProps } from './interface'
import { useRouter } from 'next/router'
import { Spinner, StyledSpinner } from '@nextui-org/react'
import Image from 'next/image'

export const CardSpaces: React.FC<ISpaceCardProps> = (props) => {
  const router = useRouter()
  const handleDetailClick = (id: number) => {
    router.push({
      pathname: '/spaces/[pid]',
      query: { pid: id },
    })
  }

  return (
    <>
      <div className="card w-96 bg-base-100 shadow-xl">
        <figure>
          <Image
            src={props.image}
            alt={props.name}
            placeholder="blur"
            blurDataURL="/assets/images/gray.png"
            width={400}
            height={300}
            className="object-cover w-96 h-[200px] rounded-t-2xl"
          />
        </figure>
        <div className="card-body">
          <h2 className="card-title">{props.name}</h2>
          <p>{props.description}</p>
          <div className="card-actions justify-end">
            <button
              className="btn btn-primary"
              onClick={() => handleDetailClick(props.id || -1)}
            >
              Detail
            </button>
          </div>
        </div>
      </div>
    </>
  )
}
