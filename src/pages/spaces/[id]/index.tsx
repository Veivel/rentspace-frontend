import React from 'react'
import type { NextPage } from 'next'
import { Detail } from '@modules/SpacesModule/Detail'

const SpacesDetail: NextPage = () => <Detail />

export default SpacesDetail
