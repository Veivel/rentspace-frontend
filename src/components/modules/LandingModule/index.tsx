import React from 'react'
import Hero from './sections/Hero'
import Image from 'next/image'
// import {HeroSection, FAQSection} from './sections
// import {} from './module-elements'
import { useAuthContext } from '@contexts'
import secureLocalStorage from 'react-secure-storage'

export const LandingModule: React.FC = () => {
  const user = JSON.parse(secureLocalStorage.getItem('user') as string)
  const jwt = JSON.parse(secureLocalStorage.getItem('jwt') as string)
  const { isAuthReady } = useAuthContext()

  function handleClick() {
    console.log('user', user)
    console.log('jwt', jwt)
    console.log('isAuthReady', isAuthReady)
  }

  return (
    <>
      <main className="relative w-full lg:px-32 md:px-16 px-3 min-h-screen">
        <Hero />
      </main>
    </>
  )
}
