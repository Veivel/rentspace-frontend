import React, { createContext, useContext, useEffect, useState } from 'react'
import secureLocalStorage from 'react-secure-storage'
import { IAuthContext, IAuthContextProps, Jwt, User } from './interface'
import axios from 'axios'
import { set } from 'react-hook-form'

const AuthContext = createContext<IAuthContext>({
  jwt: null,
  setJwt: () => {},
  user: null,
  setUser: () => {},
  isAuthReady: true,
  setIsAuthReady: () => {},
})

export const useAuthContext = () => useContext(AuthContext)

export const AuthProvider: React.FC<IAuthContextProps> = ({ children }) => {
  const [jwt, setJwt] = useState<Jwt | null>(null)
  const [user, setUser] = useState<User | null>(null)
  const [isAuthReady, setIsAuthReady] = useState(false)
  const [triggered, setTriggered] = useState(false)

  function getAccountAndRefresh(jwtObject: Jwt) {
    jwtObject.accessToken &&
      axios
        .get(`${process.env.NEXT_PUBLIC_SERVICE_MAIN_URL}/api/v1/account`, {
          headers: {
            Authorization: jwtObject?.accessToken,
          },
        })
        .then((res) => {
          setUser(res.data)
        })
        .catch((err) => {
          return axios
            .post(
              `${process.env.NEXT_PUBLIC_SERVICE_MAIN_URL}/api/v1/auth/refresh-token`,
              {
                refreshToken: jwtObject?.refreshToken,
              }
            )
            .then((res2) => {
              setJwt(res2?.data)
              getAccountAndRefresh(res2?.data)
            })
            .catch((err) => {
              secureLocalStorage.removeItem('jwt')
              setJwt(null)
              secureLocalStorage.removeItem('user')
              setUser(null)
            })
        })
  }

  useEffect(() => {
    if (!triggered) {
      setTriggered(true)
      setIsAuthReady(false)

      const parsedJwt = JSON.parse(secureLocalStorage.getItem('jwt') as string)
      if (parsedJwt) {
        setJwt(parsedJwt)
        getAccountAndRefresh(parsedJwt)
      }
      setIsAuthReady(true)
    }
  }, [])

  return (
    <AuthContext.Provider
      value={{ jwt, setJwt, user, setUser, isAuthReady, setIsAuthReady }}
    >
      {children}
    </AuthContext.Provider>
  )
}
