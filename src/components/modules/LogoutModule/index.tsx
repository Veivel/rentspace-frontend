import { useAuthContext } from '@contexts'
import { useRouter } from 'next/router'
import React, { useEffect } from 'react'
import secureLocalStorage from 'react-secure-storage'

export const LogoutModule: React.FC = () => {
  const { setJwt, setUser } = useAuthContext()
  const router = useRouter()

  useEffect(() => {
    secureLocalStorage.removeItem('jwt')
    setJwt(null)
    secureLocalStorage.removeItem('user')
    setUser(null)

    setTimeout(() => {
      router.push('/')
    }, 2000)
  }, [])

  return (
    <>
      <main className="min-h-screen">
        <div className="py-16 xl:px-64 lg:px-20 md:px-16 px-6">
          <div className="shadow-lg flex flex-col gap-y-2 py-24 px-24 bg-white rounded-3xl">
            <h1 className="text-4xl font-extrabold mx-auto mt-12">
              You have logged out.
            </h1>
            <p className="mx-auto">You will be redirected...</p>
          </div>
        </div>
      </main>
    </>
  )
}
