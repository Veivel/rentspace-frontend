import React, { useState, useEffect } from 'react'
import { useAuthContext } from '@contexts'
import { Rent } from './interface'
import axios from 'axios'
import { Button } from 'react-daisyui'

export const ProviderDashboardHistoryModule: React.FC = () => {
  const { user, jwt, isAuthReady } = useAuthContext()
  const providerId = user?.id
  const [data, setData] = useState<Rent[]>([])

  useEffect(() => {
    axios
      .get(
        `${process.env.NEXT_PUBLIC_SERVICE_MAIN_URL}/api/v1/rent/reservation/provider/${providerId}`
      )
      .then((res) => {
        setData(res.data as Rent[])
        console.log('=== data ===')
        console.log(res.data)
      })
      .catch((err) => {
        console.log('Could not fetch data.')
      })
  }, [user])

  const dummyData = [
    {
      customerId: 1,
      spaceId: 10,
    },
    {
      customerId: 25,
      spaceId: 105,
    },
    {
      customerId: 100,
      spaceId: 1000,
    },
  ]

  return (
    <>
      <div className="flex flex-col justify-center items-center">
        <h2 className="text-2xl font-bold">History Reservasi</h2>
        <table className="w-full text-center whitespace-no-wrap">
          <thead>
            <tr className="text-xs font-medium tracking-wider text-white uppercase bg-slate-500 ">
              <th className="px-6 py-4">Customer ID</th>
              <th className="px-6 py-4">Space ID</th>
            </tr>
          </thead>
          <tbody className="bg-white divide-y divide-gray-200">
            {data.map((space) => {
              return (
                <tr key={space.id}>
                  <td className="px-6 py-4">{space.customerID}</td>
                  <td className="px-6 py-4">{space.spaceID}</td>
                </tr>
              )
            })}
            {dummyData.map((space) => {
              return (
                <tr key={space.spaceId}>
                  <td className="px-6 py-4">{space.customerId}</td>
                  <td className="px-6 py-4">{space.spaceId}</td>
                </tr>
              )
            })}
          </tbody>
        </table>
      </div>
    </>
  )
}
