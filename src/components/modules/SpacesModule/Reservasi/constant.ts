export const BASE_URL = process.env.NEXT_PUBLIC_SERVICE_MAIN_URL

export const RENT_API = {
  CREATE_RENT: `${BASE_URL}/api/v1/rent/reservation/create`,
}

export const SPACES_API = {
  GET_SPACE_BY_ID: `${BASE_URL}/api/v1/space`,
}
