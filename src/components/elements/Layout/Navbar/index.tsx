import Link from 'next/link'
import React, { useEffect } from 'react'
import { Button, Navbar } from 'react-daisyui'
import { useAuthContext } from '@contexts'
import secureLocalStorage from 'react-secure-storage'

export const NavbarComponent: React.FC = () => {
  const { isAuthReady, user, jwt } = useAuthContext()

  return (
    <>
      <Navbar className="relative flex w-full h-full bg-[#353334] py-2 px-24 text-neutral-200 justify-between z-20">
        <Link className="text-lg font-black my-auto" href={'/'}>
          RentSpace
        </Link>
        <div className="hidden px-2 mx-2 navbar-center md:flex w-fit">
          <div className="flex gap-x-4 lg:gap-x-8 w-fit">
            <Link
              href="/spaces"
              className="my-auto btn btn-ghost btn-sm rounded-btn hover:scale-[1.06]"
            >
              Spaces
            </Link>
            <Link
              href="/rentlist"
              className="my-auto btn btn-ghost btn-sm rounded-btn hover:scale-[1.06]"
            >
              Rent
            </Link>
            <Link
              href="/payment"
              className="my-auto btn btn-ghost btn-sm rounded-btn hover:scale-[1.06]"
            >
              Saldo
            </Link>
            {/* <Link href="#" className="my-auto btn btn-ghost btn-sm rounded-btn hover:scale-[1.06]">
              Contact
            </Link> */}
          </div>
        </div>
        <div className="flex h-full gap-x-1">
          <button className="btn btn-ghost hover:scale-[1.06]">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 24 24"
              className="inline-block w-5 h-5 stroke-current"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth="2"
                d="M15 17h5l-1.405-1.405A2.032 2.032 0 0118 14.158V11a6.002 6.002 0 00-4-5.659V5a2 2 0 10-4 0v.341C7.67 6.165 6 8.388 6 11v3.159c0 .538-.214 1.055-.595 1.436L4 17h5m6 0v1a3 3 0 11-6 0v-1m6 0H9"
              ></path>
            </svg>
          </button>
          {isAuthReady && !user ? (
            <Button
              size="sm"
              href="/auth/login"
              className="bg-indigo-500 hover:bg-indigo-600 text-white my-auto"
            >
              Sign Up
            </Button>
          ) : (
            <></>
          )}
          {isAuthReady &&
            (user?.role === 'CUSTOMER' || user?.role === 'USER') && (
              <div className="flex justify-center items-center gap-x-4">
                <Link href="/profile">
                  <p className="my-auto ">Welcome, {user.username}.</p>
                </Link>
                <Button
                  href="/auth/logout"
                  size="sm"
                  className="bg-red-500 hover:bg-red-600 text-white my-auto"
                >
                  Logout
                </Button>
              </div>
            )}
          {isAuthReady &&
            (user?.role === 'PROVIDER' || user?.role === 'ADMIN') && (
              <div className="flex justify-center items-center gap-x-4">
                <Link href="/dashboard">
                  <p className="my-auto ">Welcome, {user.username}.</p>
                </Link>
                <Button
                  href="/auth/logout"
                  size="sm"
                  className="bg-red-500 hover:bg-red-600 text-white my-auto"
                >
                  Logout
                </Button>
              </div>
            )}
        </div>
      </Navbar>
    </>
  )
}
