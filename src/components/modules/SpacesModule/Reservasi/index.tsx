import React, { useEffect, useState, EventHandler } from 'react'
import { useRouter } from 'next/router'
import { Space, Reservation } from './interface'
import axios from 'axios'
import { useAuthContext } from '@contexts'
import { ToastContainer, toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'

export const Reservasi: React.FC = () => {
  const router = useRouter()
  const { id } = router.query
  const { user, jwt, isAuthReady } = useAuthContext()

  const [data, setData] = useState<Space>()
  useEffect(() => {
    axios
      .get(`${process.env.NEXT_PUBLIC_SERVICE_MAIN_URL}/api/v1/spaces/${id}`)
      .then((res) => {
        setData(res.data as Space)
        console.log(res.data)
      })
      .catch((err) => {
        console.log(err)
      })
  }, [id])

  const handleSubmit = () => {
    console.log(user)
    console.log(jwt)
    console.log('=== data ===')
    console.log(user?.id)
    console.log(data?.id)
    console.log(dateStart)
    console.log(dateEnd)
    if (!isAuthReady) return
    axios
      .post(
        `${process.env.NEXT_PUBLIC_SERVICE_MAIN_URL}/api/v1/rent/reservation/create`,
        {
          customerID: user?.id,
          spaceID: data?.id,
          dateStart: dateStart,
          dateEnd: dateEnd,
        },
        {
          headers: {
            Authorization: jwt?.accessToken,
          },
        }
      )
      .then((res) => {
        toast.success('Successfully reserved.')
        setTimeout(() => {
          router.push(`/spaces`)
        }, 2000)
      })
      .catch((err) => {
        console.log(err)
        toast.error('Could not reserve.')
        setTimeout(() => {
          setDateStart('')
          setDateEnd('')
        }, 1000)
      })

    return
  }

  const [dateStart, setDateStart] = useState<string>('')
  const [dateEnd, setDateEnd] = useState<string>('')
  const handleChangeDateStart = (e: React.ChangeEvent<HTMLInputElement>) => {
    setDateStart(e.target.value)
  }
  const handleChangeDateEnd = (e: React.ChangeEvent<HTMLInputElement>) => {
    setDateEnd(e.target.value)
  }

  const handleBack = () => {
    router.back()
  }

  return (
    <>
      <div className="flex flex-col justify-center items-center p-10">
        <button
          onClick={handleBack}
          className="absolute top-20 left-10 bg-slate-500 hover:bg-slate-700 hover:scale-105 duration-100 focus:scale-95 focus:bg-slate-300 px-4 py-2 rounded-xl text-white"
        >
          back
        </button>
        <div className="flex flex-col justify-center items-center bg-slate-300 px-20 py-10 rounded-md">
          <h1 className="text-2xl font-bold mb-3">Reservasi Nama Spaces</h1>
          <label className="mb-1">Date Start</label>
          <input
            value={dateStart}
            onChange={handleChangeDateStart}
            type="date"
            className="border-2 border-gray-300 rounded-md p-2 w-80 mb-5"
            required
          />
          <label className="mb-1">Date End</label>
          <input
            value={dateEnd}
            onChange={handleChangeDateEnd}
            type="date"
            className="border-2 border-gray-300 rounded-md p-2 w-80"
            required
          />
          <input
            onClick={handleSubmit}
            value="submit"
            type="submit"
            className="px-4 py-2 mt-5 bg-cyan-500 hover:bg-cyan-700 hover:scale-105 duration-75 rounded-lg"
          />
        </div>
        <ToastContainer />
      </div>
    </>
  )
}
