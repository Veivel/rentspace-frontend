export interface Reservation {
  customerID: number
  spaceID: number
  dateStart: string
  dateEnd: string
}

export interface Space {
  id: number
  name: string
  description: string
  image: string
  providerId: number
  type: string
  totalSlots: number
  rentCost: number
  approved: boolean
  openArea: boolean
}
