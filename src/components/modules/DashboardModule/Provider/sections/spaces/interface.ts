export interface Space {
  id?: number
  name: string
  providerId: number
}

export interface Rent {
  id: number
  customerID: number
  spaceID: number
  dateStart: string
  dateEnd: string
  isValid: boolean
}
