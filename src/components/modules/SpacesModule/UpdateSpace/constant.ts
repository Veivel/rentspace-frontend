export const BASE_URL = process.env.NEXT_PUBLIC_SERVICE_MAIN_URL

export const SPACES_API = {
  GET_ALL_SPACES: `${BASE_URL}/api/v1/space`,
  GET_SPACE_BY_ID: `${BASE_URL}/api/v1/space/:id`,
  CREATE_SPACE: `${BASE_URL}/api/v1/spaces/`,
  DELETE_SPACE: `${BASE_URL}/api/v1/spaces/:id`,
  UPDATE_SPACE: `${BASE_URL}/api/v1/spaces`,
  GET_ALL_EXTRA_SERVICES: `${BASE_URL}/api/v1/spaces/:spaceId/extras`,
  CREATE_EXTRA_SERVICE: `${BASE_URL}/api/v1/spaces/:spaceId/extras`,
  DELETE_EXTRA_SERVICE: `${BASE_URL}/api/v1/spaces/:spaceId/extras/:id`,
}
