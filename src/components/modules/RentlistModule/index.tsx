import React, { useEffect, useState } from 'react'
import Image from 'next/image'
import axios from 'axios'
import { useAuthContext } from '@contexts'
import { RentData, SpacesData } from './interface'

export const RentlistModule: React.FC<any> = () => {
  const { user, jwt, isAuthReady } = useAuthContext()
  if (isAuthReady && user != null) {
    const [dataFromRent, setDataFromRent] = useState<RentData[]>()
    const [dataCombinedOfRentAndSpaces, setDataCombinedOfRentAndSpaces] =
      useState<SpacesData[]>()
    const [apiCallsMade, setApiCallsMade] = useState(false)

    useEffect(() => {
      axios
        .get(
          `${process.env.NEXT_PUBLIC_SERVICE_MAIN_URL}/api/v1/rent/reservation/customer/${user?.id}`
        )
        .then((res) => {
          setDataFromRent(res.data)
        })
        .catch((err) => {
          console.log(err)
        })
    }, [])

    useEffect(() => {
      if (!apiCallsMade && dataFromRent && dataFromRent.length > 0) {
        setApiCallsMade(true)
        dataFromRent?.forEach(async (item) => {
          await axios
            .get(
              `${process.env.NEXT_PUBLIC_SERVICE_MAIN_URL}/api/v1/spaces/${item.spaceID}`
            )
            .then((res) => {
              setDataCombinedOfRentAndSpaces((dataCombinedOfRentAndSpaces) => {
                const prevArray = dataCombinedOfRentAndSpaces ?? []
                return [...prevArray, res.data]
              })
            })
            .catch((err) => {
              console.log(err)
            })
        })
      }
    }, [dataFromRent, apiCallsMade])

    const props: any = []

    dataCombinedOfRentAndSpaces?.forEach((item, index) => {
      const updatedData = item
      updatedData.dateStart = dataFromRent![index].dateStart
      updatedData.dateEnd = dataFromRent![index].dateEnd
      // eslint-disable-next-line react/prop-types
      props.push(updatedData)
    })
    return (
      <>
        <main>
          <div className="container mx-auto max-w-screen-lg py-10">
            <div className="flex justify-center items-start">
              <div className="w-2/5 text-center">
                <ProfileBlock name={user?.username} />
                <div className="h-0.5"></div>
              </div>
              <div className="w-4/5 text-start">
                <DaftarPemesanan spaces={props} />
              </div>
            </div>
          </div>
        </main>
      </>
    )
  }
  return <></>
}

export const ProfileBlock: any = (props: any) => {
  return (
    <>
      <div className="flex items-center gap-4 bg-white rounded-lg shadow-md transform -translate-x-4">
        <Image
          src="/assets/images/racoon.png"
          alt="me"
          width="64"
          height="64"
          id="racoon"
        />
        <label htmlFor="racoon" style={{ fontWeight: 'bold' }}>
          {props.name}
        </label>
      </div>
    </>
  )
}

export const DaftarPemesanan: any = (props: any) => {
  return (
    <>
      <div className="flex flex-col items-start place-content-center">
        <h1 style={{ fontSize: 24 }}>Daftar Pemesanan</h1>
        <br />
      </div>
      <div className="flex flex-col items-start bg-white rounded-lg border mx-auto py-2 px-3">
        {props.spaces.map((item: any, index: any) => (
          <>
            <div
              key={index}
              className="container items-start gap-1 bg-white rounded-lg py-2 px-2 h-20 my-2"
              style={{ boxShadow: '1px 1px 5px #CDCDCD' }}
            >
              <div className="flex justify-center items-start">
                <div className="w-4/5">
                  <h1 style={{ fontWeight: 'bold' }}>{item.name}</h1>
                  <h2>
                    {item.dateStart} / {item.dateEnd}
                  </h2>
                </div>
                <div className="w-1/5">
                  <h1>
                    {item.isValid ? 'Terverifikasi' : 'Belum Terverifikasi'}
                  </h1>
                </div>
              </div>
            </div>
          </>
        ))}
        {props.spaces.length == 0 ? (
          <>
            <div className="container h-1000 flex flex-col justify-center items-center">
              <Image
                className="mx-auto my-100 py-5"
                src="/assets/images/belanja.png"
                alt="empty"
                width="250"
                height="300"
                id="belanja"
              />
              <br />
              <h1
                className="mx-auto my-100 py-5"
                style={{ fontWeight: 'bold', fontSize: '24px' }}
              >
                Treat yourself, you deserve it!
              </h1>
            </div>
          </>
        ) : (
          <></>
        )}
      </div>
    </>
  )
}
