import { useRouter } from 'next/router'
import React, { useEffect, useState } from 'react'
import { Space } from '../interface'
import axios from 'axios'
import { Breadcrumbs } from 'react-daisyui'
import { Spinner, StyledSpinner } from '@nextui-org/react'
import Image from 'next/image'
import { useAuthContext } from '@contexts'
import { Extra } from '../interface'
import { set } from 'react-hook-form'
import toast from 'react-hot-toast'

// TODO: detail --> hit endpoint detail space by id
export const Detail: React.FC = () => {
  const [data, setData] = useState<Space | null>()
  const [extras, setExtras] = useState<Extra[] | null>()
  const [showFailedToaster, setShowFailedToaster] = useState(false)
  const [showSucceedToaster, setShowSucceedToaster] = useState(false)

  const router = useRouter()
  const { id } = router.query

  const { user, jwt, isAuthReady } = useAuthContext()

  const handleBack = () => {
    router.back()
  }
  const handleRent = () => {
    router.push(`/spaces/${id}/reservasi`)
  }
  const handleEdit = () => {
    router.push(`/spaces/${id}/update`)
  }

  useEffect(() => {
    const getData = async () => {
      if (!isAuthReady) return

      try {
        const { id } = await router.query
        const res = await axios.get(
          `${process.env.NEXT_PUBLIC_SERVICE_MAIN_URL}/api/v1/spaces/${id}`
        )
        const resExtras = await axios.get(
          `${process.env.NEXT_PUBLIC_SERVICE_MAIN_URL}/api/v1/spaces/${id}/extras`
        )
        setData(res.data)
        setExtras(resExtras.data)
        console.log('success')
      } catch (error) {
        console.log(error)
      }
    }
    getData()
  }, [id, isAuthReady])

  useEffect(() => {
    if (isAuthReady) {
      axios
        .get(`${process.env.NEXT_PUBLIC_SERVICE_MAIN_URL}/api/v1/spaces/${id}`)
        .then((res) => {
          console.log(res)
          setData(res.data as Space)
        })
        .catch((err) => {
          toast.error('Could not fetch data.')
          console.log(err)
        })
    }
  }, [isAuthReady])

  return (
    <main className="min-h-screen">
      <div className="relative flex flex-col justify-center items-center m-5 bg-slate-500 rounded-xl shadow-2xl">
        {showFailedToaster && (
          <div className="toast toast-end mb-10 mr-10">
            <div className="alert alert-error">
              <div>
                <span>Request failed.</span>
              </div>
            </div>
          </div>
        )}
        {showSucceedToaster && (
          <div className="toast toast-end mb-10 mr-10">
            <div>
              <span>Request sent.</span>
            </div>
          </div>
        )}
        <button
          onClick={handleBack}
          className="absolute top-5 left-5 bg-slate-500 hover:bg-slate-700 hover:scale-105 duration-100 focus:scale-95 focus:bg-slate-300 px-4 py-2 rounded-xl text-white"
        >
          {'◀'} Back
        </button>
        {!data ? (
          <div className="h-[40vh] w-full">
            <StyledSpinner />
          </div>
        ) : (
          <Image
            src={data?.image}
            alt={data?.name}
            width={400}
            height={300}
            className="w-full h-[40vh] rounded-t-xl object-cover"
            placeholder="blur"
            blurDataURL="/assets/images/gray.png"
          />
        )}
        {!data ? (
          <div className="flex flex-col justify-start bg-slate-200 w-full p-10 rounded-b-xl">
            <StyledSpinner />
          </div>
        ) : (
          <div className="flex flex-col justify-start bg-slate-200 w-full p-10 rounded-b-xl">
            <h1 className="text-4xl font-black text-left flex gap-x-2">
              {data?.name}
              {data?.approved ? (
                <p className="ml-3 text-lg text-left text-white bg-green-500 px-2 py-1 rounded-md">
                  Approved
                </p>
              ) : (
                <p className="ml-3 text-lg text-left text-white bg-red-500 px-2 py-1 rounded-md">
                  Not approved
                </p>
              )}
              <div className="my-auto rounded-lg bg-slate-500 p-2 text-white">
                <p className="text-lg text-left">
                  {data?.openArea ? 'Open Area' : 'Enclosed Area'}
                </p>
              </div>
            </h1>
            <div className="flex flex-row w-full gap-x-36">
              <div className="mt-3 justify-self-start">
                <p className="text-lg text-left">
                  ID Provider: {data?.providerId}
                </p>
                <p className="text-lg text-left">
                  Total Capacity: {data?.totalSlots}
                </p>
                <p className="text-lg text-left">Rent cost: {data?.rentCost}</p>
                <p className="text-lg text-left">Space Type: {data?.type}</p>
              </div>
              <p className="text-lg mt-3 justify-self-start">
                {data?.description}
              </p>
              <div className="text-lg mt-3 justify-self-start">
                <h1>
                  <b>Extra services provided:</b>
                </h1>
                <div className="grid grid-cols-3 gap-1">
                  {extras?.map((extra, idx) => (
                    <div
                      key={idx}
                      className="p-1 bg-blue-900 text-white text-sm font-light text-center rounded-2xl w-[150px]"
                    >
                      <p>{extra.name}</p>
                    </div>
                  ))}
                </div>
                {extras?.length === 0 ? '- NONE' : ''}
              </div>
              {user?.role === 'PROVIDER' && (
                <div className="flex place-items-end ml-auto">
                  <button
                    onClick={handleEdit}
                    className="text-white bg-blue-500 hover:bg-blue-700 hover:scale-105 duration-100 focus:bg-blue-400 px-4 py-2 rounded-lg mr-3"
                  >
                    Edit
                  </button>
                </div>
              )}
              {user?.role === 'CUSTOMER' && (
                <div className="flex place-items-end ml-auto">
                  <button
                    onClick={handleRent}
                    className="text-white bg-blue-500 hover:bg-blue-700 hover:scale-105 duration-100 focus:bg-blue-400 px-4 py-2 rounded-lg "
                  >
                    Rent!
                  </button>
                </div>
              )}
              {user?.role === 'USER' && (
                <div className="flex place-items-end ml-auto">
                  <button
                    onClick={handleRent}
                    className="text-white bg-blue-500 hover:bg-blue-700 hover:scale-105 duration-100 focus:bg-blue-400 px-4 py-2 rounded-lg "
                  >
                    Rent!
                  </button>
                </div>
              )}
            </div>
          </div>
        )}
      </div>
    </main>
  )
}
