export interface Space {
  id?: number
  name: string
  image: string
  description: string
  providerId: number
  type: string
  totalSlots: number
  rentCost: number
  openArea: boolean
  approved: boolean
}
export interface Rent {
  id: number
  customerID: number
  spaceID: number
  dateStart: string
  dateEnd: string
  isValid: boolean
}
