import React from 'react'
import type { NextPage } from 'next'
import { UpdateSpace } from '@modules/SpacesModule/UpdateSpace'

const SpacesDetailUpdate: NextPage = () => <UpdateSpace />

export default SpacesDetailUpdate
