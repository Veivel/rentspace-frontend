import { FooterComponent } from './Footer'
import { NavbarComponent } from './Navbar'
import { LayoutProps } from './interface'

export const Layout: React.FC<LayoutProps> = ({ children }: LayoutProps) => {
  return (
    <>
      <NavbarComponent />
      {children}
      <FooterComponent />
    </>
  )
}
