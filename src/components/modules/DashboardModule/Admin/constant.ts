export const BASE_URL = process.env.NEXT_PUBLIC_SERVICE_MAIN_URL

export const SPACES_API = {
  GET_ALL_SPACES: `${BASE_URL}/api/v1/space`,
}
