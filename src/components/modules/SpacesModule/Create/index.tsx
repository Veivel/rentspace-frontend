import React, { useState, useEffect } from 'react'
import { useRouter } from 'next/router'
import axios from 'axios'
import { useForm, SubmitHandler } from 'react-hook-form'
import { useAuthContext } from '@contexts'
import { Button, ButtonGroup, Input, Select } from 'react-daisyui'
import { ToastContainer, toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import { Extra, Space } from '../interface'

type Inputs = {
  name: string
  description: string
  image: string
  type: string
  totalSlots: number
  rentCost: number
  openArea: boolean
  approved: boolean
}

export const CreateSpaceModule: React.FC = () => {
  const router = useRouter()
  const { user, jwt, isAuthReady } = useAuthContext()

  const [data, setData] = useState<Space>()

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<Inputs>()

  const handleBack = () => {
    router.back()
  }

  const onSubmit: SubmitHandler<Inputs> = async (data) => {
    axios
      .post(`${process.env.NEXT_PUBLIC_SERVICE_MAIN_URL}/api/v1/spaces`, data, {
        headers: {
          Authorization: `${jwt?.accessToken}`,
        },
      })
      .then((res) => {
        toast.success('Space successfully created.')
      })
      .catch((err) => {
        toast.error(err.message)
      })
  }

  return (
    <>
      <div className="bg-slate-300 flex flex-row gap-x-4 justify-center items-center h-full p-10">
        <div>
          <button
            onClick={handleBack}
            className="absolute top-20 left-10 bg-slate-500 hover:bg-slate-700 hover:scale-105 duration-100 focus:scale-95 focus:bg-slate-300 px-4 py-2 rounded-xl text-white"
          >
            back
          </button>
          <h1 className="text-3xl font-bold">Update Space</h1>
          <form
            className="w-[30vw] flex flex-col"
            onSubmit={handleSubmit(onSubmit)}
          >
            <label>Name</label>
            <input
              className="mb-1 rounded-md px-3 py-2"
              defaultValue={data?.name}
              {...register('name', { required: true })}
            />
            {errors.name && <span>This field is required</span>}

            <label>Description</label>
            <input
              className="mb-1 rounded-md px-3 py-2"
              defaultValue={data?.description}
              {...register('description', { required: true })}
            />
            {errors.description && <span>This field is required</span>}

            <label>Image Link</label>
            <input
              className="mb-1 rounded-md px-3 py-2"
              defaultValue={data?.image}
              {...register('image', { required: true })}
            />
            {errors.image && <span>This field is required</span>}

            <label>Type</label>
            <Select {...register('type')} className="border border-neutral-200">
              <option value={'BALAI'}>Balai </option>
              <option value={'RUANG_RAPAT'}>Ruang Rapat </option>
              <option value={'RUANG_SIDANG'}>Ruang Sidang</option>
              <option value={'LAPANGAN'}>Lapangan </option>
              <option value={'JALAN_RAYA'}>Jalan Raya </option>
              <option value={'FASILITAS_LAIN'}>Etc.</option>
            </Select>
            {errors.type && <span>This field is required</span>}

            <label>Total Slots</label>
            <input
              type="number"
              className="mb-1 rounded-md px-3 py-2"
              defaultValue={data?.totalSlots}
              {...register('totalSlots', { required: true })}
            />
            {errors.totalSlots && <span>This field is required</span>}

            <label>Rent Cost</label>
            <input
              type="number"
              className="mb-1 rounded-md px-3 py-2"
              defaultValue={data?.rentCost}
              {...register('rentCost', { required: true })}
            />
            {errors.rentCost && <span>This field is required</span>}

            <label>Open Area</label>
            <select
              className="mb-1 rounded-md px-3 py-2"
              {...register('openArea', { required: true })}
            >
              <option value="true">Yes</option>
              <option value="false">No</option>
            </select>

            <Button type="submit">Submit</Button>
          </form>
        </div>
        <ToastContainer />
      </div>
    </>
  )
}
