import React, { useState, useEffect } from 'react'
import { useAuthContext } from '@contexts'
import { Space, Rent } from './interface'
import axios from 'axios'
import { ToastContainer, toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'

export const ProviderDashboardReservasiModule: React.FC = () => {
  const { user, jwt } = useAuthContext()
  const [data, setData] = useState<Rent[]>([])

  useEffect(() => {
    axios
      .get(
        `${process.env.NEXT_PUBLIC_SERVICE_MAIN_URL}/api/v1/rent/reservation/findAll`
      )
      .then((res) => {
        setData(res.data as Rent[])
      })
      .catch((err) => {
        console.log('Could not fetch data.')
        toast.error('Could not fetch data.')
      })
  }, [])

  const getRentId = () => {
    const inputElement = document.querySelector(
      'input[name="id-rent"]'
    ) as HTMLInputElement
    const value = inputElement.value
    return value
  }

  const handleAccept = () => {
    const id = getRentId()
    axios
      .put(
        `${process.env.NEXT_PUBLIC_SERVICE_MAIN2_URL}/api/v1/rent/reservation/validate/${id}`,
        {},
        {
          headers: {
            Authorization: jwt?.accessToken,
          },
        }
      )
      .then((res) => {
        toast.success('Reservation accepted.')
      })
      .catch((err) => {
        toast.error('Could not accept reservation.')
      })
    return
  }

  const handleReject = () => {
    const id = getRentId()
    axios
      .delete(
        `${process.env.NEXT_PUBLIC_SERVICE_MAIN2_URL}/api/v1/rent/reservation/delete/${id}`
      )
      .then((res) => {
        toast.success('Reservation rejected.')
      })
      .catch((err) => {
        toast.error('Could not reject reservation.')
      })
    return
  }

  return (
    <>
      <div className="flex flex-col justify-center items-center">
        <h2 className="text-2xl font-bold">Daftar Reservasi</h2>
        <table className="w-full text-center whitespace-no-wrap">
          <thead>
            <tr className="text-xs font-medium tracking-wider text-white uppercase bg-slate-500 ">
              <th className="px-6 py-4">Customer ID</th>
              <th className="px-6 py-4">Space ID</th>
              <th className="px-6 py-4">Action</th>
            </tr>
          </thead>
          <tbody className="bg-white divide-y divide-gray-200">
            {data.map((rent) => {
              return (
                <tr key={rent.spaceID}>
                  <input name="id-rent" type="hidden" value={rent.id} />
                  <td className="px-6 py-4">{rent.customerID}</td>
                  <td className="px-6 py-4">{rent.spaceID}</td>
                  <td>
                    <button
                      onClick={handleAccept}
                      className="px-4 py-2 text-white bg-violet-500 duration-75 hover:bg-violet-700 hover:scale-105 rounded-md mr-1"
                    >
                      Accept
                    </button>
                    <button
                      onClick={handleReject}
                      className="px-4 py-2 text-white bg-rose-700 duration-75 hover:bg-rose-700 hover:scale-105 rounded-md ml-1"
                    >
                      Reject
                    </button>
                  </td>
                </tr>
              )
            })}
          </tbody>
        </table>
        <ToastContainer />
      </div>
    </>
  )
}
