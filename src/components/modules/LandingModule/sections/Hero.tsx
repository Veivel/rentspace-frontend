import Image from 'next/image'
import { useEffect, useState } from 'react'
import { Button } from 'react-daisyui'

const LETTER_INTERVAL_IN_MILLISECONDS = 65
const KEYWORD_INTERVAL_IN_MILLISECONDS = 1500
const KEYWORDS_LIST: string[] = [
  'keynote event',
  'sports game',
  'meeting',
  'party',
  'reunion',
]

export default function Hero() {
  const [keywordIndex, setKeywordIndex] = useState<number>(-1)
  const [currentKeyword, setCurrentKeyword] = useState<string>('keynote event')

  // functional, recursive logic for keyword animation

  function switchKeyword() {
    setKeywordIndex((prev) => (prev + 1) % KEYWORDS_LIST.length)
  }

  function writeKeyword(keyword: string) {
    setTimeout(() => {
      setCurrentKeyword((prev) => prev.concat(keyword.slice(0, 1)))
      if (keyword.slice(1)) {
        writeKeyword(keyword.slice(1))
      } else {
        switchKeyword()
      }
    }, LETTER_INTERVAL_IN_MILLISECONDS)
  }

  function sliceKeyword(len: number) {
    setTimeout(() => {
      setCurrentKeyword((prev) => prev.slice(0, prev.length - 1))
      if (len > 1) {
        sliceKeyword(len - 1)
      }
    }, LETTER_INTERVAL_IN_MILLISECONDS)
  }

  useEffect(() => {
    if (keywordIndex === -1) {
      setKeywordIndex(0)
      return
    }

    const len = currentKeyword.length
    if (len >= 1) {
      setTimeout(() => {
        sliceKeyword(len)
      }, KEYWORD_INTERVAL_IN_MILLISECONDS)
    } else {
      writeKeyword(KEYWORDS_LIST[keywordIndex])
    }
  }, [keywordIndex])

  useEffect(() => {
    if (!(currentKeyword.length >= 1)) {
      switchKeyword()
    }
  }, [currentKeyword])

  return (
    <>
      <div className="relative flex flex-col gap-x-16 md:flex-row justify-center items-center min-h-[40vw] w-full">
        {/* left */}
        <div
          className="flex flex-col relative mb-10 leading-none
            text-center md:text-left 2xl:pl-[15vw]
            md:w-[50%] md:max-w-[55%] w-[100%] h-[80vw] md:h-fit mx-auto md:mx-0 md:mr-auto md:px-0 px-[10vw] sm:px-[8vw]"
        >
          <h1 className="text-6xl text-black font-black mr-3 md:my-0 my-auto">
            <span>Plan your next </span>
            <br />
            <p className="inline text-blue-darkest font-productSansBold font-black underlinable w-fit text-orange-500">
              {currentKeyword}
            </p>
            <p className="inline">.</p>
          </h1>
          <br />
          <p className="text-lg font-light leading-normal flex flex-col">
            <span>Welcome to RentSpace - find and rent any space!</span>
            <br />
            <span>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
              eiusmod tempor incididunt ut labore et dolore magna aliqua.
            </span>
            <br />
          </p>
          <div>
            <Button href="/spaces">View Spaces</Button>
          </div>
          <br />
          <br />
        </div>

        <Image
          height={320}
          width={320}
          className="
                relative lg:-mr-32 md:-mr-16 -mr-3 lg:-mt-20 md:-mt-16 -mt-12
                ml-auto md:w-[50vw] h-[50vw] w-0 rounded-l-[25%] object-cover
                "
          src="/assets/images/hall.jpeg"
          alt="hero"
        />
      </div>
    </>
  )
}
