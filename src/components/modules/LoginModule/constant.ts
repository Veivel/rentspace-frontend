import { ILoginData } from './interface'

export const DEFAULT_LOGIN_DATA: ILoginData = {
  username: '',
  password: '',
  rememberMe: false,
}
