import React from 'react'

export const HistoryTopup: React.FC = () => {
  // TODO: Write module's logic
  const history = [
    {
      date: '2021-01-01',
      amount: 100000,
      status: 'success',
    },
    {
      date: '2021-01-02',
      amount: 5000,
      status: 'failed',
    },
    {
      date: '2021-01-03',
      amount: 15000,
      status: 'success',
    },
  ]

  return (
    <>
      <div className="w-[30vw] overflow-hidden rounded-lg shadow-xs">
        <div className="w-full overflow-x-auto">
          <table className="w-full text-center whitespace-no-wrap">
            <thead>
              <tr className="text-xs font-medium tracking-wider text-white uppercase bg-slate-500 ">
                <th className="px-6 py-4">Date</th>
                <th className="px-6 py-4">Amount</th>
                <th className="px-6 py-4">Status</th>
              </tr>
            </thead>
            <tbody className="bg-white divide-y divide-gray-200">
              {history.map((item, index) => (
                <tr key={index}>
                  <td className="px-6 py-4 whitespace-no-wrap">{item.date}</td>
                  <td className="px-6 py-4 whitespace-no-wrap">
                    {item.amount}
                  </td>
                  {item.status === 'success' ? (
                    <td className="text-green-500 px-6 py-4 whitespace-no-wrap">
                      {item.status}
                    </td>
                  ) : (
                    <td className="text-red-500 px-6 py-4 whitespace-no-wrap">
                      {item.status}
                    </td>
                  )}
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
    </>
  )
}
