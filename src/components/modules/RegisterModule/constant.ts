import { IRegisterData } from './interface'

export const DEFAULT_REGISTER_DATA: IRegisterData = {
  username: '',
  password: '',
  repeatedPassword: '',
  role: 'CUSTOMER',
}
