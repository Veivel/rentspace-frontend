import React from 'react'
import type { NextPage } from 'next'
import { Reservasi } from '@modules/SpacesModule/Reservasi'

const SpacesDetailReservasi: NextPage = () => <Reservasi />

export default SpacesDetailReservasi
