import '../styles/globals.css'
import type { AppProps } from 'next/app'
import { Layout } from 'src/components/elements/Layout'
import { AuthProvider } from '@contexts'

export default function App({ Component, pageProps }: AppProps) {
  return (
    <>
      <AuthProvider>
        <Layout>
          <Component {...pageProps} />
        </Layout>
      </AuthProvider>
    </>
  )
}
