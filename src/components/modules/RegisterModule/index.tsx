import React, { ChangeEvent, useState } from 'react'
import { IRegisterData } from './interface'
import { useAuthContext } from '@contexts'
import axios from 'axios'
import { Input, Button, Select } from 'react-daisyui'
import { DEFAULT_REGISTER_DATA } from './constant'
import { useRouter } from 'next/router'
import Link from 'next/link'
import secureLocalStorage from 'react-secure-storage'
import { ToastContainer, toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'

type Role = 'CUSTOMER' | 'PROVIDER'

export const RegisterModule: React.FC = () => {
  const [form, setForm] = useState<IRegisterData>(DEFAULT_REGISTER_DATA)
  const { jwt, isAuthReady } = useAuthContext()
  const { setUser, setJwt, setIsAuthReady } = useAuthContext()
  const router = useRouter()

  function handleFormChange(
    e: ChangeEvent<HTMLInputElement | HTMLSelectElement>
  ) {
    e.preventDefault()
    setForm((prev) => ({
      ...prev,
      [e.target.name]: e.target.value,
    }))
  }

  function handleFormSubmit(e: any) {
    e.preventDefault()
    const body = {
      username: form.username,
      password: form.password,
      role: form.role,
    }

    axios
      .post(
        `${process.env.NEXT_PUBLIC_SERVICE_MAIN_URL}/api/v1/auth/register`,
        body
      )
      .then((res) => {
        const { accessToken } = res.data
        setJwt(res.data)
        secureLocalStorage.setItem('jwt', JSON.stringify(res.data))

        axios
          .get(`${process.env.NEXT_PUBLIC_SERVICE_MAIN_URL}/api/v1/account`, {
            headers: {
              Authorization: accessToken,
            },
          })
          .then((res2) => {
            setUser(res2.data)
            secureLocalStorage.setItem('user', JSON.stringify(res2.data))
          })
          .then((res) => {
            toast.success('Successfully registered.')
          })
          .then((res) => {
            setTimeout(() => {
              router.push('/')
            }, 2000)
          })
          .catch((err) => {
            toast.error('Could not register.')
          })

        axios
          .post(
            `${process.env.NEXT_PUBLIC_SERVICE_MAIN_URL}/api/v1/saldo`,
            {
              amount: 0,
            },
            {
              headers: {
                Authorization: accessToken,
              },
            }
          )
          .then((res) => {
            console.log(res)
          })
          .catch((err) => {
            console.log(err)
          })
      })
      .catch((err) => {
        toast.error('Could not register.')
      })
  }

  return (
    <>
      <main className="min-h-screen">
        <div className="py-16 xl:px-64 lg:px-20 md:px-16 px-6">
          <div className="shadow-lg flex flex-col gap-y-2 py-12 px-24 bg-white rounded-3xl">
            <h1 className="text-4xl font-extrabold mt-3 mb-7 mx-auto">
              Register
            </h1>
            <label>Username</label>
            <Input
              name="username"
              value={form?.username}
              onChange={handleFormChange}
              placeholder="Your username"
            />
            <label>Password</label>
            <Input
              name="password"
              type="password"
              value={form?.password}
              onChange={handleFormChange}
              placeholder="Your password"
            />
            <label>Repeat Password</label>
            <Input
              name="repeatedPassword"
              type="password"
              value={form?.repeatedPassword}
              onChange={handleFormChange}
              placeholder="Your password again"
            />
            <label>Role</label>
            <Select
              value={form?.role}
              className="border border-neutral-200"
              onChange={(e) =>
                setForm({ ...form, role: e.target.value as Role })
              }
            >
              <option value={'CUSTOMER'}>CUSTOMER</option>
              <option value={'PROVIDER'}>PROVIDER</option>
            </Select>
            <br />
            <Button onClick={handleFormSubmit}>Submit</Button>
            <br />
            <Link href="/auth/login" className="mx-auto">
              Already have an account? Login
            </Link>
          </div>
        </div>
        <ToastContainer />
      </main>
    </>
  )
}
