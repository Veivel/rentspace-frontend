import React, { useState } from 'react'
import axios from 'axios'
import { BASE_URL } from './constant'
import { useAuthContext } from '@contexts'
import { ToastContainer, toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'

export const Modals: React.FC = () => {
  const [value, setValue] = useState<number>(0)
  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setValue(Number(e.target.value))
  }
  const { jwt, user, isAuthReady } = useAuthContext()

  const handleSubmit = () => {
    // close the modal
    const modal = document.getElementById('my-modal-3')
    modal?.click()

    if (isAuthReady) {
      axios
        .post(
          `${process.env.NEXT_PUBLIC_SERVICE_MAIN_URL}/api/v1/saldo/topup`,
          {
            amount: value,
          },
          {
            headers: {
              Authorization: jwt?.accessToken,
            },
          }
        )
        .then((res) => {
          toast.success('Successfully top up.')
          setValue(0)
        })
        .catch((err) => {
          toast.error('Could not top up.')
          setValue(0)
        })
    }
  }

  return (
    <>
      {/* MODAL */}
      {/* The button to open modal */}
      <label htmlFor="my-modal-3" className="btn">
        Top Up
      </label>

      {/* Put this part before </body> tag */}
      <input type="checkbox" id="my-modal-3" className="modal-toggle" />
      <div className="modal">
        <div className="modal-box relative">
          <label
            htmlFor="my-modal-3"
            className="btn btn-sm btn-circle absolute right-2 top-2"
          >
            ✕
          </label>
          <h3 className="text-lg font-bold">Top Up Saldo</h3>
          <div className="mt-4 flex flex-col">
            <label className="mb-1">Amount</label>
            <input
              value={value}
              onChange={handleChange}
              type="numeric"
              placeholder="Amount"
              className="input input-bordered input-info w-full max-w-md"
              required
            />
            {/* click this button to create a request topup saldo */}
            <div onClick={handleSubmit} className="modal-action">
              <label htmlFor="my-modal-6" className="btn">
                topup!
              </label>
            </div>
          </div>
        </div>
      </div>
      <ToastContainer />
    </>
  )
}
