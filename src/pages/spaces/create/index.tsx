import React from 'react'
import type { NextPage } from 'next'
import { CreateSpaceModule } from '@modules/SpacesModule/Create'

const CreateSpace: NextPage = () => <CreateSpaceModule />

export default CreateSpace
